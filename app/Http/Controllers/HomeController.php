<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
    	   $no_of_medicine_categories=DB::table('categories')->count();
    	   $no_of_medicine_items=DB::table('medicines')->count();
    	   $no_of_executives=DB::table('users')->where('user_type',"executive")->count();
    	   //$no_of_invoices=DB::table('invoices')->count();
    	   return view('pages.index',compact('no_of_medicine_categories','no_of_medicine_items','no_of_executives'));
    }


}
