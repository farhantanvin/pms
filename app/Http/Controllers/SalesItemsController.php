<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Salesitem;
use App\Invoice;
use Session;

use Illuminate\Http\Request;

class SalesItemsController extends Controller
{
    public function index(){
        $salesitems = Salesitem::latest()->paginate(10);
        return view('pages.report.sales_item_list',compact('salesitems'));

    }


     public function dateWiseInvoiceIndex(){

    	 return view('pages.report.date_wise_invoice_index');
    }

    public function invoiceDetails($id){

    	 $items    = Salesitem::where('invoice_number',$id)->get();
    	 
    	 $invoice  = Invoice::where('invoice_number',$id)->first();
    
    	 return view('pages.report.invoice_details',compact('items','invoice'));
  

    }

}
